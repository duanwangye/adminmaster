//引入公共样式
import "./styles/default.css";
import "element-ui/lib/theme-chalk/index.css";

//引入公共框架组件
import Vue from "vue";
import Vuex from 'vuex';
import VueRouter from "vue-router";
import ElementUI from "element-ui";
import Echarts from "echarts";
import Ajax from "./functions/ajax.js";
import Store from "./store.js";

//引入创建的路由列表
import Routes from "./router";

//引入路由容器
import App from "./App.vue";

Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(Echarts);
Vue.use(Vuex);
Vue.prototype.$ajax = Ajax;

new Vue({
  el: "#app",
  router: new VueRouter({
    routes: Routes,
    mode: "history"
  }),
  store: new Vuex.Store(Store),
  render: create => create(App)
});
