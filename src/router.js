/*
  当前max：40
  注意：
  路由的name使用命名使用“route + 数字”的方式进行连接，name未来也将不变，作为路由的唯一标识，这样今后修改路由路径时，name就无需再变动。
  为避免数字重复，新建或者修改路由name时，都要从当前max之后的数字+1形式添加，并将“当前max”值手动更新到目前路由所用最大值。
*/

import Index from "@/components/pages/index";
import Login from "@/components/pages/login";
import Err404 from "@/components/pages/404";

import StorerList from "@/components/pages/storer/list";
import StorerEdit from "@/components/pages/storer/edit";
import StorerAccountList from "@/components/pages/storer/account-list";

import MemberList from "@/components/pages/member/list";
import MemberDetail from "@/components/pages/member/detail";

import AddUser from "@/components/pages/add-user";

import ProductList from "@/components/pages/product/list";
import ProductEdit from "@/components/pages/product/edit";
import ProductAdd from "@/components/pages/product/add";
import ProductAuditing from "@/components/pages/product/auditing";
import ProductApply from "@/components/pages/product/apply";
import ProductDetail from "@/components/pages/product/detail";

import TypeList from "@/components/pages/type/list";
import TypeAdd from "@/components/pages/type/add";
import TypeDetail from "@/components/pages/type/detail";
import TypeEdit from "@/components/pages/type/edit";

import OrderList from "@/components/pages/order/list";

import ImageList from "@/components/pages/image/list";
import ImageEdit from "@/components/pages/image/edit";
import ImageAdd from "@/components/pages/image/add";

import BargainAdd from "@/components/pages/bargain/add";
import BargainEdit from "@/components/pages/bargain/edit";
import BargainList from "@/components/pages/bargain/list";
import BargainLauncherList from "@/components/pages/bargain/launcher-list";
import BargainHelperList from "@/components/pages/bargain/helper-list";

import SmsList from "@/components/pages/sms/list";
import SmsAdd from "@/components/pages/sms/add";

import AgentOther from "@/components/pages/agent/other";
import AgentAccountList from "@/components/pages/agent/account-list";

import MasterOrderList from "@/components/pages/master/order-list";
import MasterAccountList from "@/components/pages/master/account-list";
import MasterCheckList from "@/components/pages/master/check-list";

import ArticleList from "@/components/pages/article/list";
import ArticleEdit from "@/components/pages/article/edit";
import ArticleAdd from "@/components/pages/article/add";
import ArticleDetail from "@/components/pages/article/detail";

export default [
  { name: "route1", path: "/index", component: Index, meta: { index: "1", user: "super" } },
  { name: "route2", path: "/login", component: Login },
  { name: "route3", path: "", component: Index, meta: { index: "1", user: "super" } },
  { name: "route4", path: "*", component: Err404 },

  { name: "route5", path: "/storer/list", component: StorerList, meta: { index: "5", user: "super" } },
  { name: "route6", path: "/storer/edit", component: StorerEdit , meta: { index: "5", user: "super" }},
  { name: "route7", path: "/storer/account-list", component: StorerAccountList , meta: { index: "5", user: "super" }},

  { name: "route8", path: "/member/list", component: MemberList, meta: { index: "4", user: "super" } },
  { name: "route9", path: "/member/detail", component: MemberDetail, meta: { index: "4", user: "super" } },

  { name: "route10", path: "/add-user", component: AddUser, meta: { user: "super" } },

  { name: "route11", path: "/product/list", component: ProductList, meta: { index: "6-1", user: "super" } },
  { name: "route12", path: "/product/edit", component: ProductEdit, meta: { index: "6-1", user: "super" } },
  { name: "route13", path: "/product/add", component: ProductAdd, meta: { index: "6-1", user: "super" } },
  { name: "route14", path: "/product/auditing", component: ProductAuditing, meta: { index: "6-2", user: "super" } },
  { name: "route15", path: "/product/apply", component: ProductApply, meta: { index: "6-2", user: "super" } },
  { name: "route16", path: "/product/detail", component: ProductDetail, meta: { index: "6-1", user: "super" } },

  { name: "route17", path: "/type/list", component: TypeList, meta: { index: "2", user: "super" } },
  { name: "route18", path: "/type/add", component: TypeAdd, meta: { index: "2", user: "super" } },
  { name: "route19", path: "/type/detail", component: TypeDetail, meta: { index: "2", user: "super" } },
  { name: "route20", path: "/type/edit", component: TypeEdit, meta: { index: "2", user: "super" } },
  
  { name: "route21", path: "/order/list", component: OrderList, meta: { index: "7", user: "super" } },

  { name: "route22", path: "/image/list", component: ImageList, meta: { index: "3", user: "super" } },
  { name: "route23", path: "/image/add", component: ImageAdd, meta: { index: "3", user: "super" } },
  { name: "route24", path: "/image/edit", component: ImageEdit, meta: { index: "3", user: "super" } },

  { name: "route25", path: "/bargain/add", component: BargainAdd, meta: { index: "8", user: "super" } },
  { name: "route26", path: "/bargain/edit", component: BargainEdit, meta: { index: "8", user: "super" } },
  { name: "route27", path: "/bargain/list", component: BargainList, meta: { index: "8", user: "super" } },
  { name: "route28", path: "/bargain/launcher-list", component: BargainLauncherList, meta: { index: "8", user: "super" } },
  { name: "route29", path: "/bargain/helper-list", component: BargainHelperList, meta: { index: "8", user: "super" } },

  { name: "route30", path: "/sms/list", component: SmsList, meta: { index: "9", user: "super" } },
  { name: "route31", path: "/sms/add", component: SmsAdd, meta: { index: "9", user: "super" } },

  { name: "route32", path: "/agent/other", component: AgentOther, meta: { index: "2", user: "agent" } },
  { name: "route33", path: "/agent/account-list", component: AgentAccountList, meta: { index: "1", user: "agent" } },

  { name: "route34", path: "/master/order-list", component: MasterOrderList, meta: { index: "1", user: "master" } },
  { name: "route35", path: "/master/account-list", component: MasterAccountList, meta: { index: "2", user: "master"} },
  { name: "route36", path: "/master/check-list", component: MasterCheckList, meta: { index: "3", user: "master"} },

  { name: "route37", path: "/article/list", component: ArticleList, meta: { index: "10", user: "super" } },
  { name: "route38", path: "/article/edit", component: ArticleEdit, meta: { index: "10", user: "super"} },
  { name: "route39", path: "/article/add", component: ArticleAdd, meta: { index: "10", user: "super"} },
  { name: "route40", path: "/article/detail", component: ArticleDetail, meta: { index: "10", user: "super" } }
];
