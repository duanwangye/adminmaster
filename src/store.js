//默认自动获取本地用户登录缓存
export default {
  modules: {
    user: {
      namespaced: true,
      state: {
        token: "",
        userId: "",
        userType: ""
      },
      mutations: {
        setUserSign(state) {
          let info = {};
          if (window.localStorage.lkUserInfo) {
            info = JSON.parse(window.localStorage.lkUserInfo);
          } else if (window.sessionStorage.lkUserInfo) {
            info = JSON.parse(window.sessionStorage.lkUserInfo);
          }
          state.token = info.token;
          state.userId = info.userId;
          state.userType = info.userType
        },
        removeUserSign(state) {
          window.localStorage.removeItem("lkUserInfo");
          window.sessionStorage.removeItem("lkUserInfo");
          state.token = "";
          state.userId = "";
          state.userType = "";
        }
      }
    }
  }
};